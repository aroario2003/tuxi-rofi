.POSIX:

PREFIX = ~/.local
MANDIR = /usr/share/man

all: install

tuxi-rofi:

install:
	@mkdir -p ${PREFIX}/bin
	@cp -p tuxi-rofi ${DESTDIR}${PREFIX}/bin/tuxi-rofi
	@sudo cp -p tuxi-rofi.1 ${DESTDIR}${MANDIR}/man1

uninstall:
	@rm -f ${DESTDIR}${PREFIX}/bin/tuxi-rofi
	@rm -f ${DESTDIR}${MANDIR}/man1/tuxi-rofi.1*

.PHONY: all install uninstall
