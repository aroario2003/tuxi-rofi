<div align="center"> 

# Tuxi-Rofi 

</div>

<div align="center">

A script to make TUXI more functional and now a voice assistant 

</div>

<div align="center" width="700px"> 

![](./media/tuxi-rofi.mp4)

</div>

## What is it?

This project was inspired by Bugswritter and his recent development by the name of `tuxi`. Tuxi is a tool to search google for simple queries and get reutrned results immediately. I set out to extend the functionality of Tuxi and make it more accessible to everyones workflow, even when they are not in a terminal. The result I came up with is a simple bash script, `tuxi-rofi`, which instead of printing output to the terminal will print output to a notification and the links associated with the given query to rofi.  

## Disclaimer

To be clear, I take no credit for tuxi in the slightest and all I intend to do is extend it's funtionality. This script is still in the early stages of development and I plan on extending its functionality much further in the near future. My bash scripting is far from good, so any constructive criticsm is appreciated. 

## Dependencies

* tuxi
* rofi
* notify-send
* dialog 
* nerd-dictation (check github for more information on installation)

## Installation

 You can use the [Makefile](./Makefile) to install `tuxi-rofi`. Also, `tuxi-rofi` does include the `-i` flag which will install tuxi for you if you desire, otherwise checkout the [tuxi](https://github.com/Bugswriter/tuxi) repository for information about how to install it. 

## Dmenu functionality

Since I use rofi as my run launcher, I did not make this script with dmenu users in mind, However, you can use dmenu with the `-D` or `-a` flag. The difference between the two flags is that `-D` gives you the notification with the result to your query and the links related to the query whereas the `-a` flag only gives you a notification. Finally, I have now added the `-l` flag so that dmenu will only give links when the option is specified.

## Voice Assistant Functionality 

My initial idea for `tuxi-rofi` was to have it be a voice assistant of sorts on Linux. However, this was never implemented because I could not find any good speech to text programs that were free and open source. On the other hand, the other day I found a speech to text program called `nerd-dictation`, this program is free and open source and now allows `tuxi-rofi` to have a voice assistant functionality. You now can just speak your query and than `tuxi-rofi` will give your results. 
**NOTE**: This feature maybe buggy as a result of `nerd-dictation` not understanding some vocabulary, so please speak clearly!

## Environment Variables

In order to use the environment variables that this script supports as mentioned in the **Updates** section you must export them in your `.zshrc` , `.bashrc` or `config.fish`. 

## More information

This script now has a man page that will be put in its corresponding directory after running `make install`. You may access this man page by running `man tuxi-rofi`, this will help you learn how to use the script in a more efficient manner according to your workflow. You can also use `-h` or `--help` for a quick glance at all the wonderful features and options that this script has to offer. 

## Updates

As of today **4/17/21**, I have added multiple new flags and long format flags, which means that now some short flags also support their long form. In addition, I have added the ability to use environment variables with tuxi-rofi. These are as follows: `TUXI_ROFI_BROWSER`, `TUXI_ROFI_TIMEOUT` and `TUXI_ROFI_DESTINATION`. I have now added a make file to the script that it can easily be installed. To do so, `cd` into the `tuxi-rofi` directory and type `make install`, this will in turn install tuxi-rofi to the correct directory. Also, you **MUST** have `.local/bin` in your $PATH in order to execute the script. In the coming days and weeks, I will be working on a man page for this scirpt so that it is easier to use. However, for now you can refer to either the `-h` flag or the `--help` flag for assistance using the script.

As of today **4/28/21**, I have added the ability to add bookmarks so that you can visit websites again if you like the link that tuxi-rofi gives you as output. I have also added an ncurses menu so that configuration of Environment variables is easier for everyone who uses this script. I have also added two new flags one long and one short to display the bookmarks which are saved in the `bookmarks.txt` in the directory where you set your `TUXI_ROFI_DESTINATION` (<u>if not set, it is your home directory</u>). The flags are `-B` and `--bookmarks`.

As of today **5/3/21**, I have finally added a man page for `tuxi-rofi` and have made the first release **0.0.1**. There will be more releases coming in the future once enough new features are added and bugs are possible resolved. I have also added one new enviroment varible to the script called `HIST_DEST`, this is the location where the history file will be stored on your system (**NOTE** this feature is not fully functional just yet). This is only the begginning of this script and their will be much more to come. 

As of today **8/22/21** `tuxi-rofi` is back and better than ever with new voice assistant functionality. In order to activate it use the `-A` or `-N` option and than simply speak to tuxi-rofi and it will either give you a notification with the result or give you links related to the result depending on the flag given. 

## License

This project is licensed under the [GPL-3.0](./LICENSE).
